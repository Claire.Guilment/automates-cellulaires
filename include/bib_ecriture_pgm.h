#ifndef BIB_ECRITURE
#define BIB_ECRITURE

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <math.h>

/* Déclaration des fonctions pour la création d'images PGM */
FILE* creation_fichier(unsigned int nombre_magique, char* nom_de_fichier);
void ecrire_les_donnees_ascii(FILE* fichier,unsigned int largeur,unsigned int longueur, int* donnees);
void ecrire_dans_l_entete(FILE* fichier, unsigned int largeur, unsigned int hauteur,unsigned int nombre_magique,unsigned int valeur_max);
void ecrire_les_donnees_binaire(FILE* fichier,unsigned int largeur,unsigned int longueur,int* donnees, int nbEtat);


#endif
