#ifndef AUTOMATE_H
#define AUTOMATE_H

#include "bib_ecriture_pgm.h"
#include "bib_fonction_menu.h"

struct affichage_{
  unsigned int u_regle;
  unsigned int iterations;
  unsigned int largeur;
  unsigned int nb_Etat;
  unsigned int val_max;
  unsigned int img_pgm;
  unsigned int nb_magique;
  int* regles;
  int* matrice;

};


typedef struct affichage_ aff;
typedef struct affichage_* pt_aff;


/* Déclaration des fonctions pour automate */

unsigned int get_aff_u_regle(pt_aff automate);
unsigned int get_aff_iterations(pt_aff automate);
unsigned int get_aff_largeur(pt_aff automate);
unsigned int get_aff_nb_Etats(pt_aff automate);
int* get_aff_regles(pt_aff automate);
int* get_aff_matrice(pt_aff automate);




void initialisation(pt_aff);
void affichage_struct(pt_aff);
void affichage_struct_somme(pt_aff);
void convert(int* tab,unsigned int number);
void voisinage(pt_aff, int*, int*);
void voisinageSomme(pt_aff, int*);
void ajouter_int_matrice(pt_aff,unsigned int,unsigned int,unsigned int);
void regle_random(int **,unsigned int);
void initialisation_random(pt_aff,int);


pt_aff creation_aff(unsigned int,int*, unsigned int,unsigned int, unsigned int, unsigned int,unsigned int);
void free_aff(pt_aff);
void affichage_aff1(pt_aff);
void affichage_aff2(pt_aff);
void affichage_terminal_ou_pgm(pt_aff);
unsigned int cast_regle_generique_unsigned_int(int* regleGenerique,int nbEtat);
pt_aff lecture_fichier(const FILE* fichier);


#endif
