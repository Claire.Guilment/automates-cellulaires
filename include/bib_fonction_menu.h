#ifndef BIB_MENU_FONCTION
#define BIB_MENU_FONCTION

#include <stdlib.h>
#include <stdio.h>

void menuChoix(int*);
void menu1(int* ,int*,int * largeur, int* choix_pgm,int* encodage, int* nb_magique);
void menu2(int* ,int*,int * largeur, int* choix_pgm,int* encodage, int* nb_magique);
void nettoyagebuffer();
void gestion_pgm(int* iterations,int * largeur, int* choix_pgm,int* encodage,int* nb_magique);

#endif
