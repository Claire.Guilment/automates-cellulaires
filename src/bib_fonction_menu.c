/**
* @file bib_fonction_menu.c
* @author Claire Guilment
* @author Corentin Hervochon
* @brief
* @version 0.1
* @date 2019-10-04
*
* @copyright Copyright (c) 2019
*
**/


#include "bib_fonction_menu.h"


/**
* @brief fonction d'affichage qui demande à l'utilisateur de choisir quel type
* d'automate cellulaire il veut, il y a une vérification de la valeur d'entrée
* @param choix un nombre entre 1 et 2 suivant ce que l'on veut
**/
void menuChoix(int* choix){
  printf("############# BONJOUR #############\n\n");
  printf("Voici le menu :\n");
  printf("(1)-Automates cellulaires élémentaires (à deux états)\n");
  printf("(2)-Le triangle de Sierpinski (multi états)\n\n");
  printf("Quel est votre choix : ");
  scanf("%d", choix);
  while(*choix!=1 && *choix!=2){
    printf("Désolé, ce menu n'existe pas. Veuillez recommencer :");
    nettoyagebuffer();
    scanf("%d", choix);
  }
  nettoyagebuffer();
}


/**
* @brief fonction d'affichage qui demande à l'utilisateur une regle entiere comprise entre
* 0 et 255
* @param regle une regle entiere
* @param iterations le nombre d'iterations
* @param largeur la laugeur de l'automate
* @param choix_pgm
* @param encodage
* @param nb_magique un nombre pour connaître le type de fichier (ASCII ou binaire)
**/
void menu1(int* regle,int* iterations,int * largeur, int* choix_pgm,int* encodage, int* nb_magique){
  printf("Choisissez votre règle : ");
  scanf("%d", regle);
  while(*regle>255){
    printf("La règle ne doit pas excéder 255. Veuillez recommencer :");
    nettoyagebuffer();
    scanf("%d", regle);
  }
  nettoyagebuffer();
  gestion_pgm( iterations, largeur,  choix_pgm, encodage,  nb_magique);

}


/**
* @brief fonction d'affichage qui demande à l'utilisateur de choisir le nombre d'états
* maximum pour une cellule et il doit être supérieur à 2
* @param nbEtat nombre d'états maximums que peut prendre l'automate
* @param iterations le nombre d'itérations
* @param largeur la largeur de l'automate
* @param choix_pgm
* @param encodage
* @param nb_magique un nombre pour connaître le type de fichier (ASCII ou binaire)
**/
void menu2(int* nbEtat, int* iterations,int * largeur, int* choix_pgm,int* encodage, int* nb_magique){
  printf("Choisissez le nombre d'états maximum possible pour une cellule : ");
  scanf("%d", nbEtat);
  while(*nbEtat<=2){
    printf("Le nombre d'états ne peut être inférieur ou égal à 2. Veuillez recommencer :");
    nettoyagebuffer();
    scanf("%d", nbEtat);
  }
  // nettoyagebuffer();
  gestion_pgm( iterations, largeur,  choix_pgm, encodage,  nb_magique);

}


void nettoyagebuffer(){
  int c;
  while((c = getchar())!='\n' && c!=EOF){

  }
}

/**
* @brief fonction permettant de traiter les demandes de l'utilisateur sur l'affichage
* s'il souhaite une image PGM ou un affichage terminal. Elle permet aussi la personnalisation
* de la largeur de l'automate et de l'encodage de l'image PGM (ASCII ou binaire)
* @param largeur la largeur de l'automate
* @param choix_pgm
* @param encodage
* @param nb_magique un nombre pour connaître le type de fichier (ASCII ou binaire)
**/
void gestion_pgm(int* iterations,int * largeur, int* choix_pgm,int* encodage, int* nb_magique){

  printf("Choisisez le nombre d'itérations : ");
  scanf("%d", iterations);
  while(*iterations==0){
    printf("Le nombre d'itération ne peut être égale à 0. Veuillez recommencer :");
    nettoyagebuffer();
    scanf("%d", iterations);
  }
  nettoyagebuffer();
  printf("Choisisez la largeur de votre automate : ");
  scanf("%d", largeur);
  while(*largeur==0){
    printf("La largeur ne peut être égale à 0. Veuillez recommencer :");
    nettoyagebuffer();
    scanf("%d", largeur);
  }
  nettoyagebuffer();
  printf("Souhaitez vous :\n");
  printf("(1) - Un affichage terminal ?\n");
  printf("(2) - Une image PGM ?\n");
  scanf("%d",choix_pgm);
  while(*choix_pgm!=1 && *choix_pgm!=2){
    printf("Désolé, cette option n'existe pas. Veuillez recommencer :");
    nettoyagebuffer();
    scanf("%d", choix_pgm);
  }
  nettoyagebuffer();
  if(*choix_pgm>1){
    printf("Souhaitez vous :\n");
    printf("(1) - Un fichier PGM encodé en binaire ?\n");
    printf("(2) - Un fichier PGM encodé en ASCII ?\n");
    scanf("%d",encodage);
    while(*encodage!=1 && *encodage!=2){
      printf("Désolé, cette option n'existe pas. Veuillez recommencer :");
      nettoyagebuffer();
      scanf("%d", encodage);
    }
    nettoyagebuffer();
    *choix_pgm = 1;
    if(*encodage==1){
      *nb_magique=5;
    }else{
      *nb_magique=2;
    }
  }else{
    *choix_pgm=0;
    *encodage=0;
    *nb_magique=0;
  }
}
