/**
 * @file automate.h
 * @author Claire Guilment
 * @author Corentin Hervochon
 * @brief
 * @version 0.1
 * @date 2019-10-05
 *
 * @copyright Copyright (c) 2019
 *
 **/

#include "automate.h"



// implémentation des fonctions
/**
 * @brief Get the aff u regle object
 *
 * @param automate pointeur de la structure
 * @return unsigned int
 **/
unsigned int get_aff_u_regle(pt_aff automate){return automate->u_regle;}

/**
 * @brief Get the aff iterations object
 *
 * @param automate pointeur de la structure
 * @return unsigned int
 **/
unsigned int get_aff_iterations(pt_aff automate){return automate->iterations;}

/**
 * @brief Get the aff largeur object
 *
 * @param automate pointeur de la structure
 * @return unsigned int
 **/
unsigned int get_aff_largeur(pt_aff automate){return automate->largeur;}

/**
 * @brief Get the aff nb Etats object
 *
 * @param automate pointeur de la structure
 * @return unsigned int
 **/
unsigned int get_aff_nb_Etats(pt_aff automate){return automate->nb_Etat;}

/**
 * @brief Get the aff regles object
 *
 * @param automate pointeur de la structure
 * @return int*
 **/
int* get_aff_regles(pt_aff automate){return automate->regles;}

/**
 * @brief Get the aff matrice object
 *
 * @param automate pointeur de la structure
 * @return int*
 **/
int* get_aff_matrice(pt_aff automate){return automate->matrice;}


/**
 * @brief fonction qui initialise la matrice de l'automate celle ci est à 0 au départ
 *
 * @param automate pointeur de la structure
 **/
void initialisation(pt_aff automate){
  automate->matrice[automate->largeur/2]=1;
}


/**
 * @brief affichage du contenu de la structure
 *
 * @param automate pointeur de la structure
 **/
void affichage_struct(pt_aff automate){
  for (unsigned int i = 0; i < automate->iterations; i++) {
    printf("| %d\t| \t", i);
    for (unsigned int j = 0; j < automate->largeur; j++) {
      if (automate->matrice[i*automate->largeur+j]==0) {
        printf(" ");
      }
      else if (automate->matrice[i*automate->largeur+j]==1) {
        printf("X");
      }
    }
    printf("\n");
  }

}


/**
 * @brief prend en paramètre un tableau vide et un entier compris entre 0
 * et 255. La fonction vérifie si number est bien compris entre 0 et
 * 255 avant de convertir le nombre en binaire.
 *
 * @param tab un tableau vide
 * @param number un entier compris entre 0 et 255
 **/
void convert(int* tab,unsigned int number){
  if(number>255){
    printf("ERREUR : fonction convert -- NUMBER DONNÉ EN PARAMETRE > 255 \n");
    exit(EXIT_FAILURE);
  }
  int inter = number;
  for(int j=0;j<8;j++){
    tab[8-1-j] = inter%2;
    inter=inter/2;
  }
}


/**
 * @brief prend en paramètres un automate, des règles et la conversion binaire
 * modifie la matrice de l'automate. ne renvoie rien.
 *
 * @param automate pointeur de la structure
 * @param tabRegles un tableau avec la règle dedans
 * @param tabVal un tableau avec les valeurs associé à sa règle
 **/
void voisinage(pt_aff automate, int* tabRegles, int* tabVal){
  for (unsigned int i = 1; i < automate->iterations; i++) {

    for (unsigned int j = 0; j < automate->largeur; j++) {
      int numbTemp = 0;
      //début de ligne
      if(j==0){
        if(automate->matrice[(i-1)*automate->largeur+(automate->largeur)]==1){
          numbTemp+=100;
        }
        if(automate->matrice[(i-1)*automate->largeur+(j+1)]==1){
          numbTemp+=1;
        }
      }
      //fin de ligne
      else if(j==automate->largeur){
        if(automate->matrice[(i-1)*automate->largeur+(j-1)]==1){
          numbTemp+=100;
        }
        if(automate->matrice[(i-1)*automate->largeur+0]==1){
          numbTemp+=1;
        }
      }
      //cas classique
      else{
        if(automate->matrice[(i-1)*automate->largeur+(j-1)]==1){
          numbTemp+=100;
        }
        if(automate->matrice[(i-1)*automate->largeur+(j+1)]==1){
          numbTemp+=1;
        }
      }
      if(automate->matrice[(i-1)*automate->largeur+j]==1){
        numbTemp+=10;
      }

      for(int k = 0; k < 8; k++){
        if(numbTemp == tabRegles[k]){
          ajouter_int_matrice(automate, i, j, tabVal[k]);
        }
      }
    }
  }

}


/**
 * @brief Fonction créée pour insérer une valeur dans la partie matrice de
 * l'automate. Elle prend en paramètre l'automate, un numéro de ligne,
 * un numéro de colonne et la valeur.
 *
 * @param atm le pointeur de la structure
 * @param ligne ligne d'insertion
 * @param colonne colonne d'insertion
 * @param valeur entier que l'utilisateur souhaite insérer dans la structure
 **/
void ajouter_int_matrice(pt_aff atm,unsigned int ligne,unsigned int colonne,unsigned int valeur){
  atm->matrice[ ligne * atm -> largeur + colonne] = valeur;

}


/**
 * @brief Fonction de création d'une structure automate.
 *
 * @param regle regle en decimal
 * @param regleConvertieEnbinaire
 * @param iteration hauteur de l'automate
 * @param largeur largeur de l'automate
 * @param nb_Etat nombre d'états maximums que peut prendre l'automate
 * @param img_pgm valeur : 0 ou 1 (choix de l'utilisateur). Si l'utilisateur souhaite une image PGM ce paramètre est à 1.
 * @param nb_magique valeur : 2 ou 5. Pour générer une image PGM soit en ascii ou en binaire.
 * @return pt_aff
 **/
pt_aff creation_aff(unsigned int regle ,int* regleConvertieEnbinaire, unsigned int iteration,unsigned int largeur, unsigned int nb_Etat,unsigned int img_pgm,unsigned int nb_magique){
  pt_aff affichage = (pt_aff) malloc(sizeof(aff));
  affichage->u_regle = regle;
  affichage->regles = regleConvertieEnbinaire;
  affichage->iterations = iteration;
  affichage->largeur = largeur;
  affichage->nb_Etat = nb_Etat;
  affichage->val_max = nb_Etat-1;
  affichage->img_pgm =img_pgm;
  affichage->nb_magique = nb_magique;
  affichage->matrice = (int*)calloc(largeur * iteration , sizeof(int));
  return affichage;
}


/**
 * @brief Fonction de destruction d'une structure (Désallocation de la mémoire)
 *
 *
 * @param aff automate.
 **/
void free_aff(pt_aff aff){
  free(aff->matrice);
  aff->matrice =NULL;
  free(aff->regles);
  aff->regles=NULL;
  free(aff);
  aff=NULL;
}


/**
 * @brief Fonction affichant la structure totale avec le nb d'itération, la règle convertie
 * et la structure matricielle.
 *
 * @param affichage structure
 **/
void affichage_aff1(pt_aff affichage){
  printf("La règle en décimal : %d\n", affichage->u_regle);
  printf("La règle convertie en binaire :");

  for (size_t i = 0; i < 8; i++) {
    printf("%d", affichage->regles[i]);
  }
  printf("\n");
  printf("Le nombre d'itérations : %d\n",affichage->iterations);
  affichage_struct(affichage);
}


/**
 * @brief fonction d'affichage qui affiche les données rentrée par l'utilisateur et l'automate
 *  correspondant
 * @param automate structure
 **/
void affichage_aff2(pt_aff automate){
  printf("Le nombre d'états : %d\n",automate->nb_Etat);
  printf("La règle : ");
  for(unsigned int i=0;i<(3*((automate->nb_Etat)-1)+1);i++){
    printf("%d", automate->regles[i]);
  }
  printf("\n");
  printf("Le nombre d'itérations : %d\n",automate->iterations);
  affichage_struct_somme(automate);
}


/**
 * @brief fonction permettant à l'utilisateur de décider s'il souhaite
 * un affichage dans son terminal ou une image PGM.
 * @param automate structure
 **/

void affichage_terminal_ou_pgm(pt_aff automate){
  if(automate->img_pgm==0){
    if(automate->nb_Etat>2){
      affichage_aff2(automate);
    }else{
      affichage_aff1(automate);
    }
  }else{
    //générer une image en PGM
    if(automate->nb_magique==5 || automate->nb_magique==2){
      FILE * fichier;
      fichier = creation_fichier(automate->nb_magique,"image.pgm");
      ecrire_dans_l_entete(fichier,automate->largeur,automate->iterations,automate->nb_magique,automate->val_max);
      if(automate->nb_magique==5){
        ecrire_les_donnees_binaire(fichier,automate->largeur,automate->iterations,automate->matrice, automate->nb_Etat);
      }else{
        ecrire_les_donnees_ascii(fichier,automate->largeur,automate->iterations,automate->matrice);
      }
      printf("Création d'un fichier image.pgm effectuée\n");
      fclose(fichier);
    }
  }
}

/**
 * @brief fonction qui fait la somme des 3 voisions du dessus de la cellule et remplit la
 *  cellule pour toutes les cellules de l'automate
 * @param automate pointeur de la structure
 * @param tab un tableau avec les valeurs de la regle
 **/
void voisinageSomme(pt_aff automate,int* tab){
  for (unsigned int m = 1; m < automate->iterations; m++) {

    for (unsigned int c = 0; c < automate->largeur; c++) {
      unsigned int sommeVoisin = 0;
      //début de ligne
      if(c==0){
        sommeVoisin += automate->matrice[(m-1)*automate->largeur+(automate->largeur)];
        sommeVoisin += automate->matrice[(m-1)*automate->largeur+(c+1)];
      }
      //fin de ligne
      else if(c==automate->largeur){
        sommeVoisin += automate->matrice[(m-1)*automate->largeur+(c-1)];
        sommeVoisin += automate->matrice[(m-1)*automate->largeur+0];
      }
      //cas classique
      else{
        sommeVoisin += automate->matrice[(m-1)*automate->largeur+(c-1)];
        sommeVoisin += automate->matrice[(m-1)*automate->largeur+(c+1)];
      }
      sommeVoisin += automate->matrice[(m-1)*automate->largeur+c];

      ajouter_int_matrice(automate, m, c, tab[sommeVoisin]);
    }

  }
}


/**
 * @brief fonction d'affichage qui affiche la matrice, il remplace les 0 par " "
 * et affiche les valeurs de l'automate
 * @param automate pointeur de la structure
 **/
void affichage_struct_somme(pt_aff automate){
  for (unsigned int i = 0; i < automate->iterations; i++) {
    printf("| %d\t| \t", i);
    for (unsigned int j = 0; j < automate->largeur; j++) {
      if (automate->matrice[i*automate->largeur+j]==0) {
        printf(" ");
      }
      else{
        printf("%d",automate->matrice[i*automate->largeur+j]);
      }
    }
    printf("\n");
  }

}


/**
 * @brief
 *
 * @param regleGenerique
 * @param nbEtat
 **/
void regle_random(int ** regleGenerique,unsigned int nbEtat){
  srand(time(NULL));
  for(unsigned int k = 0; k < (3*(nbEtat-1)+1);k++){
    (*regleGenerique)[k]=rand()%nbEtat;
  }
}


/**
 * @brief fonction qui rempli la première ligne de l'automate avec des valeurs aléatoire
 *  entre 0 et la valeur de nbEtat
 * @param automate pointeur de la structure
 * @param nbEtat le nombre maximum d'états que peut prendre une cellule
 * @param regleGenerique
 **/
void initialisation_random(pt_aff automate,int nbEtat){
  srand(time(NULL));
  for(unsigned int i = 0; i < automate->largeur;i++){
    automate->matrice[i]=rand()%nbEtat;
  }

}


/**
 * @brief fonction qui transforme une règle générique en unsigned int
 *
 * @param regleGenerique règle aléatoire générée avec le nombre d'états
 * @param nbEtat le nombre d'états maximum que peut prendre une cellule
 * @return unsigned int
 **/
unsigned int cast_regle_generique_unsigned_int(int* regleGenerique,int nbEtat){
  unsigned int res=0;
  unsigned int taille = (3*(nbEtat-1))+1;
  for (size_t i = 0; i < taille; i++) {
    res += regleGenerique[i]*pow(10,taille-i);
  }
  return res;
}


/**
 * @brief fonction qui lite le contenu d'un fichier de configuration donné en entrée.
 *  Vérifie si le fichier de configuration est correct
 * @param fichier fichier donné en argument
 * @return pt_aff
 **/
pt_aff lecture_fichier(const FILE* fichier) {
  enum etats { INIT, MAGIC_NUMBER_FOUND, HEIGHT_FOUND,WIDTH_FOUND, NB_ETAT_MAX_FOUND,MAX_VAL_FOUND, REGLE_FOUND,IMG_PGM_FOUND,ERROR };
  enum etats etat = INIT ;
  const int MAX_LENGTH = 4096;

  unsigned int nb_iterations;
  unsigned int largeur;
  unsigned int nb_etats;
  unsigned int regle;
  unsigned int nombre_magique;
  unsigned int img_pgm;
  int * regle_en_binaire = (int *) malloc (8*sizeof(int));


  char* buf = malloc(sizeof(char)*MAX_LENGTH);
  bool lireEntete = true;

  while(lireEntete) {

    fgets(buf, MAX_LENGTH, (FILE *) fichier);

    while(strncmp(buf,"#",1) == 0)
    fgets(buf, MAX_LENGTH, (FILE *) fichier);

    switch(etat) {

      case INIT:
      if(sscanf(buf,"NOMBRE_MAGIQUE = P%u", &nombre_magique) == 1) etat = MAGIC_NUMBER_FOUND;
      break;
      case MAGIC_NUMBER_FOUND:
      if(sscanf(buf,"HAUTEUR = %u", &nb_iterations) == 1)
      {etat = HEIGHT_FOUND;
      }else{ lireEntete = false;}
      break;
      case HEIGHT_FOUND:
      if(sscanf(buf,"LARGEUR = %u", &largeur) == 1)
      {etat = WIDTH_FOUND;
      }else{ lireEntete = false;}
      break;
      case WIDTH_FOUND:
      if(sscanf(buf,"NB_ETAT_MAX = %u", &nb_etats) == 1) {
        etat = NB_ETAT_MAX_FOUND;
      }
      else lireEntete = false;
      break;
      case NB_ETAT_MAX_FOUND:
      if(sscanf(buf,"REGLE = %u", &regle) == 1)
      {etat = REGLE_FOUND;
        //gestion généricité à faire
        // check si nb etat >2
        convert(regle_en_binaire,regle);
      }else  lireEntete = false;
      break;
      case REGLE_FOUND:
      if(sscanf(buf,"IMG_PGM = %u", &img_pgm) == 1)
      {
      etat = IMG_PGM_FOUND;}
      lireEntete = false;
      break;
      default:
      etat = ERROR;
      lireEntete = false;
    }

  }


  free(buf);

  if(etat != IMG_PGM_FOUND) {
    fprintf(stdout, "Entete fichier configuration incorrecte, evaluation terminee a l'etat %d\n", etat);
    exit(EXIT_FAILURE);
  }
  return creation_aff(regle,regle_en_binaire,nb_iterations,largeur,nb_etats,img_pgm,nombre_magique);

}
