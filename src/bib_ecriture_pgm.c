/**
 * @file bib_ecriture_pgm.c
 * @author Claire Guilment
 * @author Corentin Hervochon
 * @brief
 * @version 0.1
 * @date 2019-10-04
 *
 * @copyright Copyright (c) 2019
 *
 **/

#include "bib_ecriture_pgm.h"

/**
 * @brief Fonction de création de fichier. Cette fonction ouvre un fichier en mode écriture.
 * le fichier est ouvert en w, si de l'ascii compte être inscrit dedans, sinon il est ouvert
 * en wb pour une écriture des données en binaire.
 *
 * @param nombre_magique le nombre magique en début de fichier PGM
 * @param nom_de_fichier le nom du fichier que l'on veut créer. ex "nomdufichier.pgm"
 * @return FILE*
 **/
FILE* creation_fichier(unsigned int nombre_magique, char* nom_de_fichier){
  //ASCII
  FILE* fichier;
  if(nombre_magique==2){
    fichier = fopen(nom_de_fichier,"w");
  }
  //BINAIRE
  else{
    fichier = fopen(nom_de_fichier,"wb");
  }
  if(fichier==NULL){
    printf("ERREUR : fonction creation_fichier -- FICHIER NULL \n");
    exit(EXIT_FAILURE);
  }
  return fichier;
}


/**
 * @brief Fonction prenant en paramètres un fichier, la largeur et la hauteur des données, un nombre magique
 * et la valeur max que peut prendre les données. Elle écrit dans le fichier donné son entête propre à tous les fichiers PGM.
 *
 * @param fichier Fichier dans lequel l'utilisateur souhaite écrire
 * @param largeur la largeur de l'automate
 * @param hauteur la hauteur de l'automate
 * @param nombre_magique le nombre correspond au type de fichier
 * @param valeur_max la valeur maximum des pixels
 **/
void ecrire_dans_l_entete(FILE* fichier, unsigned int largeur, unsigned int hauteur,unsigned int nombre_magique,unsigned int valeur_max){
  fprintf(fichier, "P%u\n",nombre_magique);
  fprintf(fichier, "%d %d\n",largeur,hauteur);
  fprintf(fichier, "%d\n", valeur_max);
}


/**
 * @brief Fonction prenant en paramètres un fichier, la longueur et la largeur des données et un pointeur de pointeur de données.
 * Elle écrit dans le fichier donné, en ASCII : noir pour les 0 et blanc pour les 1.
 * @param fichier Fichier dans lequel l'utilisateur souhaite écrire
 * @param largeur la largeur de l'automate
 * @param longueur la hauteur de l'automate
 * @param donnees
 **/
void ecrire_les_donnees_ascii(FILE* fichier,unsigned int largeur,unsigned int longueur,int* donnees){
  for (size_t i = 0; i < longueur*largeur; i++) {
    fprintf(fichier, "%d  ", donnees[i]);
    if(i%largeur==largeur-1){
      fprintf(fichier, "\n");
    }
  }
}


/**
 * @brief Fonction prenant en paramètres un fichier, la longueur et la largeur des données, un pointeur de pointeur de données et un nombre d'états.
 * Elle écrit dans le fichier donné, en binaire, en étalonnant des nuances de gris en fonction du nombre d'états -1.
 *
 * @param fichier Fichier dans lequel l'utilisateur souhaite écrire
 * @param largeur la largeur de l'automate
 * @param longueur la hauteur de l'automate
 * @param donnees
 * @param nbEtat nombre d'états maximums que peut prendre l'automate
 **/
void ecrire_les_donnees_binaire(FILE* fichier,unsigned int largeur,unsigned int longueur,int* donnees, int nbEtat){
  for (size_t i = 0; i < longueur*largeur; i++) {
    fprintf(fichier,"%c", (donnees[i] * 255) /  (nbEtat-1));
  }
}
