/**
* @file main.c
* @author Claire Guilment
* @author Corentin Hervochon
* @brief
* @version 0.1
* @date 2019-10-05
*
* @copyright Copyright (c) 2019
*
**/

#include "main.h"


/**
* @brief fonction principale du programme pour générer l'exécutable
*
* @param argc nombre d'arguments
* @param argv tableau d'arguments
* @return int
**/
int main(int argc, char const *argv[]) {

  int regles_en_dur[8] = {111,110,101,100,11,10,1,0};


  if (argc>1){
    // L'utilisateur fournit un fichier de configuration en parametre.

    printf("Fichier de configuration détecté: ");
    printf("%s\n", argv[1]);
    FILE* fichier_conf = fopen(argv[1],"r");
    printf("Ouverture du fichier passé en argument\n");
    pt_aff automate = lecture_fichier(fichier_conf);
    initialisation(automate);
    voisinage(automate,regles_en_dur,get_aff_regles(automate));
    affichage_terminal_ou_pgm(automate);
    fclose(fichier_conf);
    free(automate);
  }


  else{
    // L'utilisateur n'a pas fournit de fichier de configuration en parametre.
    int choix;
    int regle;
    int iterations;
    int largeur;
    int choix_pgm;
    int encodage;
    int nombre_magique;
    int nbEtat = 2;

    menuChoix(&choix);
    if(choix == 1){
      int* regle_en_binaire = (int *) malloc (8*sizeof(int));
      menu1(&regle, &iterations,&largeur,&choix_pgm,&encodage,&nombre_magique);
      pt_aff aff_;
      convert(regle_en_binaire,regle);
      aff_ = creation_aff(regle,regle_en_binaire,iterations,largeur,nbEtat,choix_pgm,nombre_magique);
      initialisation(aff_);
      voisinage(aff_, regles_en_dur,get_aff_regles(aff_));
      affichage_terminal_ou_pgm(aff_);
      free_aff(aff_);
      regle_en_binaire = NULL;
    }
    else if(choix == 2){
      int* regleGenerique;
      menu2(&nbEtat, &iterations,&largeur,&choix_pgm,&encodage,&nombre_magique);
      regleGenerique = (int*)malloc((3*(nbEtat-1)+1)*sizeof(int));
      regle_random(&regleGenerique,nbEtat);
      pt_aff aff2;
      unsigned int regle_uns= cast_regle_generique_unsigned_int(regleGenerique,nbEtat);
      aff2 = creation_aff( regle_uns,regleGenerique,iterations,largeur,nbEtat,choix_pgm,nombre_magique);
      initialisation_random(aff2,get_aff_nb_Etats(aff2));
      voisinageSomme(aff2, get_aff_regles(aff2));
      affichage_terminal_ou_pgm(aff2);
      free_aff(aff2);
      regleGenerique=NULL;
    }
    else{
      menuChoix(&choix);
    }
  }
  return 0;
}
