# compilateur
CC := gcc
# options de compilation
# CFLAGS := -std=c99 -Wall -Wextra -pedantic -ggdb -Wno-unused-but-set-parameter -Wno-unused-variable -Wno-unused-parameter -Wno-abi
CFLAGS := -std=c99 -Wall -Wextra -pedantic -ggdb -Wno-unused-parameter -Wno-unused-variable -Wno-unused-parameter -Wno-abi -iquote "include"

# règle de compilation --- exécutables
all : ./bin/automate

./bin/automate : ./obj/automate.o ./obj/bib_ecriture_pgm.o  ./obj/bib_fonction_menu.o ./obj/main.o
	$(CC) $(CFLAGS) -o $@ $^ -lm

./obj/%.o: ./src/%.c
	$(CC) $(CFLAGS) -o $@ -c $<

memoire : ./bin/automate
	valgrind --leak-check=full ./bin/automate

# options de compilation
clean:
	rm ./obj/*.o ./bin/automate

run: ./bin/automate
	./bin/automate


run_conf: ./bin/automate ./conf/conf.txt
	./bin/automate ./conf/conf.txt
