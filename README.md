# Automates Cellulaires

## Licence 3 - projet réalisé en binôme.

##### AUTEURS : Corentin HERVOCHON et Claire GUILMENT

Projet en C sur un principe simplifié d'un automate à une dimension.

Objectifs : 
* manipuler des pointeurs 
* manipuler des structures
* structurer les fichiers en .c et .h
* comprendre et adapter un makefile au projet
* lecture et écriture de fichiers (images PGM et fichiers textuels)